import requests
from sys import exit
from time import time, sleep
from datetime import datetime
from random import random, randint
import argparse



def current_time_ms():
    return int(round(time() * 1000))


# Time Defaults
time_interval = 1  # 1 second frequency

# Default device credentials
script_device_id = "drmx9wbtyw5f"
script_device_credential = "Tj8FciNtBgE0EGgJ"

# MQTT defaults
rest_host = "http://172.20.199.7"
rest_port = 8083
rest_endpoint = "/api/rest/v1/edgedevice/data"

# Argument Parser
arg_parser = argparse.ArgumentParser(description='Create real-time sensor data via REST API')
arg_parser.add_argument('--host', type=str, default=rest_host, help='REST Service host')
arg_parser.add_argument('--port', type=int, default=rest_port, help='REST Service port')
arg_parser.add_argument('--interval', type=int, default=time_interval, help='Time difference between '
                                                                            'two sensor data timestamp in second')


def publish_rest(url, data):
    resp = requests.post(url, json=data)
    print("Response: " + str(resp))


def generate_random_sensor_values(device_id):
    light = randint(80, 90) + randint(-5, 5) * random()
    gas = randint(350, 370) + randint(-30, 30) * random()

    timestamp = current_time_ms()
    light_data = {"time": timestamp, "deviceId": device_id, "sensorType": "Light", "value": light}
    gas_data = {"time": timestamp, "deviceId": device_id, "sensorType": "Gas", "value": gas}
    return light_data, gas_data


def approve_to_continue():
    yes = {'yes', 'y', 'ye'}
    no = {'no', 'n'}

    while True:
        choice = input("Do you want to continue to publish real-time data with the options above? "
                       "('y/yes' or 'n/no')\n").lower()
        if choice in yes:
            return True
        elif choice in no:
            return False
        else:
            print("Please respond with 'y/yes' or 'n/no'!\n")


def print_and_get_arguments():
    arg_parser.print_help()
    args = arg_parser.parse_args()
    host = args.host
    port = args.port
    interval = args.interval  # time difference between two sensor data timestamp in seconds
    url = host + ":" + str(port) + rest_endpoint

    info = """
    \nReal-time data generation will start with these options:\n
    Device ID: %s
    REST Host: %s
    REST Port: %s
    REST Endpoint: %s
    REST Url: %s
    
    Interval: %d second(s) between two sensor data.
    """ % (script_device_id, host, port, rest_endpoint, url, interval)
    print(info)

    return url, host, port, interval


def main():
    url, host, port, interval = print_and_get_arguments()

    if not approve_to_continue():
        print("Exiting...")
        exit()

    data_start_time = datetime.now()
    print("Generating sensor data...\t" + str(data_start_time))
    while True:
        try:
            light_data, gas_data = generate_random_sensor_values(script_device_id)

            publish_rest(url, light_data)
            publish_rest(url, gas_data)

            sleep(interval)
        except KeyboardInterrupt:
            data_end_time = datetime.now()
            print("\nStopped!\t\t\t" + str(data_end_time))
            print("Total time:\t\t\t" + str(data_end_time - data_start_time))
            exit()


if __name__ == "__main__":
    main()
