import Adafruit_DHT
import paho.mqtt.client as paho
from sys import exit
import json
from time import time, sleep
from datetime import datetime
from random import random, randint

DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 4

# Time Defaults
time_interval = 1

# Default device credentials
script_device_id = "c735cefd8ea1"
script_device_credential = "vkRnc2a6R6ARuo6A"

# MQTT defaults
mqtt_broker_host = "192.168.43.176"
mqtt_broker_port = 1883
mqtt_data_topic = "data/device/"


def current_time_ms():
    return int(round(time() * 1000))


def on_publish(client, userdata, result):
    print(script_device_id + ": Data published. " + str(result))
    pass


def publish_mqtt(mqtt_client, data):
    ret = mqtt_client.publish(mqtt_data_topic + script_device_id, data)


def generate_payload(humidity, temperature, timestamp):
    humidity_data = json.dumps(
        {"time": timestamp, "deviceId": script_device_id, "sensorType": "Humidity", "value": humidity}).encode('utf8')
    temperature_data = json.dumps(
        {"time": timestamp, "deviceId": script_device_id, "sensorType": "Temperature", "value": temperature}).encode('utf8')
    return humidity_data, temperature_data


def main():
    print("\nConnecting MQTT Broker...\t" + str(datetime.now()))
    mqtt_client = paho.Client(script_device_id)
    mqtt_client.on_publish = on_publish
    mqtt_client.connect(mqtt_broker_host, mqtt_broker_port)

    data_start_time = datetime.now()
    print("Started sending sensor data...\t" + str(data_start_time))
    while True:
        try:
            humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
            timestamp = current_time_ms()

            if humidity and temperature:
                humidity_data, temperature_data = generate_payload(humidity, temperature, timestamp)
                publish_mqtt(mqtt_client, humidity_data)
                publish_mqtt(mqtt_client, temperature_data)

            sleep(1)
        except KeyboardInterrupt:
            mqtt_client.disconnect()
            data_end_time = datetime.now()
            print("\nStopped!\t\t\t" + str(data_end_time))
            print("Disconnecting from MQTT")
            print("Total time:\t\t\t" + str(data_end_time - data_start_time))
            exit()


if __name__ == "__main__":
    main()
