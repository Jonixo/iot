from sys import exit
import paho.mqtt.client as paho
import json
import time
from datetime import datetime
from random import random, randint
import argparse



def current_time_ms():
    return int(round(time.time() * 1000))


time_now_ms = current_time_ms()
second_ms = 1000
minute_ms = second_ms * 60
hour_ms = minute_ms * 60
day_ms = hour_ms * 24
time_interval = 60 * 3  # Three minutes time difference between two sensor data timestamp in seconds.
start_day = 366
end_day = 30

# Default device credentials
script_device_id = "54ef3a00d93c"
script_device_credential = "cBOhpQ1mwgTeQ9DE"

# MQTT defaults
mqtt_broker_host = "172.20.199.6"
mqtt_broker_port = 1883
mqtt_data_topic = "data/device/"

# Argument Parser
arg_parser = argparse.ArgumentParser(description='Create One Year Sensor Data via MQTT')
arg_parser.add_argument('--host', type=str, default=mqtt_broker_host, help='MQTT Broker host')
arg_parser.add_argument('--port', type=int, default=mqtt_broker_port, help='MQTT Broker port')
arg_parser.add_argument('--interval', type=int, default=time_interval, help='Time difference between '
                                                                            'two sensor data timestamp in second')
arg_parser.add_argument('--start', type=int, default=start_day, help='Start generating data from how many days '
                                                                     'before current time')
arg_parser.add_argument('--end', type=int, default=end_day, help='End generating data after how many days '
                                                                 'from current time')


def on_publish(client, data, result):
    pass


def publish_mqtt(mqtt_client, data):
    ret = mqtt_client.publish(mqtt_data_topic + script_device_id, data)


def generate_random_sensor_values(timestamp, device_id):
    volt = randint(220, 230) + randint(-5, 5) * random()
    pressure = randint(1000, 1250) + randint(-100, 100) * random()
    temperature = randint(40, 50) + randint(-10, 10) * random()

    volt_data = json.dumps(
        {"time": timestamp, "deviceId": device_id, "sensorType": "Voltage", "value": volt}).encode('utf8')
    pressure_data = json.dumps(
        {"time": timestamp, "deviceId": device_id, "sensorType": "Pressure", "value": pressure}).encode('utf8')
    temperature_data = json.dumps(
        {"time": timestamp, "deviceId": device_id, "sensorType": "Temperature", "value": temperature}).encode('utf8')
    return volt_data, pressure_data, temperature_data


def approve_to_continue():
    yes = {'yes', 'y', 'ye'}
    no = {'no', 'n'}

    while True:
        choice = input("Do you want to continue to generate data with the options above? "
                       "('y/yes' or 'n/no')\n").lower()
        if choice in yes:
            return True
        elif choice in no:
            return False
        else:
            print("Please respond with 'y/yes' or 'n/no'!\n")


def print_and_get_arguments():
    arg_parser.print_help()
    args = arg_parser.parse_args()
    host = args.host
    port = args.port
    interval = args.interval * second_ms  # time difference between two sensor data timestamp in millisecond
    start_time = time_now_ms - day_ms * args.start
    end_time = time_now_ms + day_ms * args.end
    num_of_data = (end_time - start_time) / interval

    info = """
    \nData generation will start with these options:\n
    Device ID: %s
    MQTT Host: %s
    MQTT Port: %s
    MQTT Topic: %s
    
    Interval: %d seconds between two sensor data.
    Start day: Minus %d days from now.
    End day: Plus %d days from now.
    
    %d sensor data will be generated for each sensor (Temperature, Pressure, Volt).
    Total Data: %d will be generated.
    """ % (script_device_id, host, port, mqtt_data_topic+script_device_id, args.interval, args.start, args.end,
           num_of_data, num_of_data*3)
    print(info)

    return host, port, interval, start_time, end_time


def main():
    host, port, interval, start_time, end_time = print_and_get_arguments()

    if not approve_to_continue():
        print("Exiting...")
        exit()

    print("\nConnecting MQTT Broker...\t" + str(datetime.now()))
    mqtt_client = paho.Client(script_device_id)
    mqtt_client.on_publish = on_publish
    mqtt_client.connect(host, port)

    data_start_time = datetime.now()
    print("Generating sensor data...\t" + str(data_start_time))
    while start_time < end_time:
        volt_data, pressure_data, temperature_data = generate_random_sensor_values(start_time, script_device_id)

        publish_mqtt(mqtt_client, volt_data)
        publish_mqtt(mqtt_client, pressure_data)
        publish_mqtt(mqtt_client, temperature_data)

        start_time += interval

    data_end_time = datetime.now()
    print("Finished!\t\t\t" + str(data_end_time))
    print("Total time:\t\t\t" + str(data_end_time - data_start_time))


if __name__ == "__main__":
    main()
