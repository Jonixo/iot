import paho.mqtt.client as paho
from sys import exit
import json
from time import time, sleep
from datetime import datetime
from random import random, randint
import argparse



def current_time_ms():
    return int(round(time() * 1000))


# Time Defaults
time_interval = 1

# Default device credentials
script_device_id = "54ef3a00d93c"
script_device_credential = "cBOhpQ1mwgTeQ9DE"

# MQTT defaults
mqtt_broker_host = "172.20.199.6"
mqtt_broker_port = 1883
mqtt_data_topic = "data/device/"

# Argument Parser
arg_parser = argparse.ArgumentParser(description='Create real-time sensor data via MQTT')
arg_parser.add_argument('--host', type=str, default=mqtt_broker_host, help='MQTT Broker host')
arg_parser.add_argument('--port', type=int, default=mqtt_broker_port, help='MQTT Broker port')
arg_parser.add_argument('--interval', type=int, default=time_interval, help='Time difference between '
                                                                            'two sensor data timestamp in second')


def on_publish(client, userdata, result):
    print(script_device_id + ": Data published. " + str(result))
    pass


def publish_mqtt(mqtt_client, data):
    ret = mqtt_client.publish(mqtt_data_topic + script_device_id, data)


def generate_random_sensor_values(device_id):
    volt = randint(220, 230) + randint(-5, 5) * random()
    pressure = randint(1000, 1250) + randint(-100, 100) * random()
    temperature = randint(40, 50) + randint(-10, 10) * random()

    timestamp = current_time_ms()
    volt_data = json.dumps(
        {"time": timestamp, "deviceId": device_id, "sensorType": "Voltage", "value": volt}).encode('utf8')
    pressure_data = json.dumps(
        {"time": timestamp, "deviceId": device_id, "sensorType": "Pressure", "value": pressure}).encode('utf8')
    temperature_data = json.dumps(
        {"time": timestamp, "deviceId": device_id, "sensorType": "Temperature", "value": temperature}).encode('utf8')
    return volt_data, pressure_data, temperature_data


def approve_to_continue():
    yes = {'yes', 'y', 'ye'}
    no = {'no', 'n'}

    while True:
        choice = input("Do you want to continue to publish real-time data with the options above? "
                       "('y/yes' or 'n/no')\n").lower()
        if choice in yes:
            return True
        elif choice in no:
            return False
        else:
            print("Please respond with 'y/yes' or 'n/no'!\n")


def print_and_get_arguments():
    arg_parser.print_help()
    args = arg_parser.parse_args()
    host = args.host
    port = args.port
    interval = args.interval  # time difference between two sensor data timestamp in seconds

    info = """
	\nReal-time data generation will start with these options:\n
	Device ID: %s
	MQTT Host: %s
	MQTT Port: %s
	MQTT Topic: %s

	Interval: %d second(s) between two sensor data.
	""" % (script_device_id, host, port, mqtt_data_topic + script_device_id, args.interval)
    print(info)

    return host, port, interval


def main():
    host, port, interval = print_and_get_arguments()

    if not approve_to_continue():
        print("Exiting...")
        exit()

    print("\nConnecting MQTT Broker...\t" + str(datetime.now()))
    mqtt_client = paho.Client(script_device_id)
    mqtt_client.on_publish = on_publish
    mqtt_client.connect(host, port)

    data_start_time = datetime.now()
    print("Generating sensor data...\t" + str(data_start_time))
    while True:
        try:
            volt_data, pressure_data, temperature_data = generate_random_sensor_values(script_device_id)

            publish_mqtt(mqtt_client, volt_data)
            publish_mqtt(mqtt_client, pressure_data)
            publish_mqtt(mqtt_client, temperature_data)

            sleep(interval)
        except KeyboardInterrupt:
            mqtt_client.disconnect()
            data_end_time = datetime.now()
            print("\nStopped!\t\t\t" + str(data_end_time))
            print("Disconnecting from MQTT")
            print("Total time:\t\t\t" + str(data_end_time - data_start_time))
            exit()


if __name__ == "__main__":
    main()
