package com.centralroot.edgemqttservice.config;

import static com.centralroot.edgemqttservice.common.Queues.MQTT_EDGE_DATA_TOPIC;

import com.centralroot.edgemqttservice.mqtt.subscriber.MQTTSubscriberBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageListener implements Runnable{

	@Autowired
	MQTTSubscriberBase subscriber;
	
	@Override
	public void run() {
		while(true) {
			subscriber.subscribeMessage(MQTT_EDGE_DATA_TOPIC);
		}
		
	}

}
