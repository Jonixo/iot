package com.centralroot.edgemqttservice.mqtt.subscriber;

import static com.centralroot.common.messaging.Queues.TOPIC_EDGE_DEVICE;

import com.centralroot.common.model.EdgeData;
import com.centralroot.edgemqttservice.config.MQTTConfig;
import com.centralroot.edgemqttservice.messaging.MessageSender;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MQTTSubscriber extends MQTTConfig implements MqttCallback, MQTTSubscriberBase {

	@Autowired
	private MessageSender messageSender;

	@Autowired
	ObjectMapper objectMapper;

	private String brokerUrl = null;
	final private String colon = ":";
	final private String clientId = "edge-mqtt-service-client";

	private MqttClient mqttClient = null;
	private MqttConnectOptions connectionOptions = null;
	private MemoryPersistence persistence = null;

	private static final Logger logger = LoggerFactory.getLogger(MQTTSubscriber.class);

	public MQTTSubscriber() {
		this.config();
	}

	@Override
	public void connectionLost(Throwable cause) {
		logger.info("Connection Lost");

	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		//JSONObject jsonmsg = new JSONObject(new String(message.getPayload()));
		EdgeData edgeData = objectMapper.readValue(new String(message.getPayload()), EdgeData.class);
		messageSender.sendMessage(TOPIC_EDGE_DEVICE, "", edgeData);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		// Leave it blank for subscriber
	}

	@Override
	public void subscribeMessage(String topic) {
		try {
			this.mqttClient.subscribe(topic, this.qos);
		} catch (MqttException me) {
			me.printStackTrace();
		}
	}

	public void disconnect() {
		try {
			this.mqttClient.disconnect();
		} catch (MqttException me) {
			logger.error("ERROR", me);
		}
	}

	@Override
	protected void config(String broker, Integer port, Boolean ssl, Boolean withUserNamePass) {

		String protocal = this.TCP;
		if (true == ssl) {
			protocal = this.SSL;
		}

		this.brokerUrl = protocal + this.broker + colon + port;
		this.persistence = new MemoryPersistence();
		this.connectionOptions = new MqttConnectOptions();

		try {
			this.mqttClient = new MqttClient(brokerUrl, clientId, persistence);
			this.connectionOptions.setCleanSession(true);
			if (true == withUserNamePass) {
				if (password != null) {
					this.connectionOptions.setPassword(this.password.toCharArray());
				}
				if (userName != null) {
					this.connectionOptions.setUserName(this.userName);
				}
			}
			this.mqttClient.connect(this.connectionOptions);
			this.mqttClient.setCallback(this);
		} catch (MqttException me) {
			me.printStackTrace();
		}

	}

	@Override
	protected void config() {

		this.brokerUrl = this.TCP + this.broker + colon + this.port;
		this.persistence = new MemoryPersistence();
		this.connectionOptions = new MqttConnectOptions();
		try {
			this.mqttClient = new MqttClient(brokerUrl, clientId, persistence);
			this.connectionOptions.setCleanSession(true);
			this.mqttClient.connect(this.connectionOptions);
			this.mqttClient.setCallback(this);
		} catch (MqttException me) {
			me.printStackTrace();
		}
	}

}
