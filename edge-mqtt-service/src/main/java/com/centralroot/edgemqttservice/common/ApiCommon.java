package com.centralroot.edgemqttservice.common;

public class ApiCommon {
    final public static String baseUrl = "/api/mqtt/v1/";
    final public static String edgeDeviceEndPoint = baseUrl + "edgedevice";
}
