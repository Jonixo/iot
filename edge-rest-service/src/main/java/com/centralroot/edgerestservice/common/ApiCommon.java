package com.centralroot.edgerestservice.common;

public class ApiCommon {
    final public static String baseUrl = "/api/rest/v1/";
    final public static String edgeDeviceEndPoint = baseUrl + "edgedevice";
}
