package com.centralroot.edgerestservice.controller;

import com.centralroot.common.model.EdgeData;
import com.centralroot.edgerestservice.service.AsyncRabbitService;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.centralroot.edgerestservice.common.ApiCommon.edgeDeviceEndPoint;

@RestController
@RequestMapping(edgeDeviceEndPoint + "/data")
public class EdgeDataController {

    @Autowired
    private AsyncRabbitService rabbitService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void sendData(@RequestBody EdgeData edgeData) {
        Preconditions.checkNotNull(edgeData);
        rabbitService.sendToServices(edgeData);
    }
}
