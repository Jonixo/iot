package com.centralroot.edgerestservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdgeRestServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(EdgeRestServiceApplication.class, args);
    }

}
