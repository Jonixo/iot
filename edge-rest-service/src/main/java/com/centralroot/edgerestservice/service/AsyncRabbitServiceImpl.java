package com.centralroot.edgerestservice.service;

import static com.centralroot.common.messaging.Queues.TOPIC_EDGE_DEVICE;

import com.centralroot.common.model.EdgeData;
import com.centralroot.edgerestservice.messaging.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncRabbitServiceImpl implements AsyncRabbitService {

  @Autowired
  private MessageSender messageSender;

  @Override
  @Async("asyncExecutor")
  public void sendToServices(EdgeData edgeData) {
    messageSender.sendMessage(TOPIC_EDGE_DEVICE, "", edgeData);
  }
}
