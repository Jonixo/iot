package com.centralroot.edgerestservice.service;

import com.centralroot.common.model.EdgeData;

public interface AsyncRabbitService {
  void sendToServices(EdgeData edgeData);
}
