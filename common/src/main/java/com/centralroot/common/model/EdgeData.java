package com.centralroot.common.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName(value="EdgeData")
public class EdgeData {
    private long time;
    private String deviceId;
    private String sensorType;
    private double value;
}
