package com.centralroot.common.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
@Builder
@JsonRootName(value="AlarmConfigInfo")
public class AlarmConfigInfo {
  private String configId;
  private String level;
  private String deviceId;
  private String sensorType;
  private String username;
  private double value;
  private String type;
}
