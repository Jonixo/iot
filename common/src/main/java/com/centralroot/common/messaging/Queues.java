package com.centralroot.common.messaging;

public class Queues {
    public static final String TOPIC_EDGE_DEVICE = "centralroot.fanout.edgedevice";
    public static final String QUEUE_ALARM_EDGE_DATA = "centralroot.fanout.alarmservice.queue";
    public static final String QUEUE_BACKEND_EDGE_DATA = "centralroot.fanout.backendservice.queue";
    public static final String QUEUE_LOGSTASH_EDGE_DATA = "centralroot.fanout.logstash.queue";

    public static final String TOPIC_ALARM = "centralroot.direct.alarm";
    public static final String QUEUE_ALARM_CONFIG = "centralroot.direct.alarmconfig.queue";
    public static final String QUEUE_ALARM_CONFIG_DEL = "centralroot.direct.alarmconfigdelete.queue";
    public static final String QUEUE_ALARM_EVENT = "centralroot.direct.alarmevent.queue";
}
