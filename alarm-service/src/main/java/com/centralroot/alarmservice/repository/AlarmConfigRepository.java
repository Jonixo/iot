package com.centralroot.alarmservice.repository;

import com.centralroot.alarmservice.entities.UserAlarmConfig;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlarmConfigRepository extends MongoRepository<UserAlarmConfig, String> {
  void deleteByConfigId(String configId);
}
