package com.centralroot.alarmservice.config;

import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_CONFIG;
import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_CONFIG_DEL;
import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_EVENT;
import static com.centralroot.common.messaging.Queues.TOPIC_ALARM;
import static com.centralroot.common.messaging.Queues.TOPIC_EDGE_DEVICE;
import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_EDGE_DATA;

import com.centralroot.alarmservice.messaging.MessageHandler;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

@Configuration
@EnableRabbit
public class MessagingConfig implements RabbitListenerConfigurer {

    @Value("${spring.rabbitmq.host}")
    private String host;

    @Value("${spring.rabbitmq.username}")
    private String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Value("${spring.rabbitmq.listener.simple.concurrency}")
    private int concurrency;

    @Value("${spring.rabbitmq.listener.simple.max-concurrency}")
    private int maxConcurrency;

    @Bean
    public FanoutExchange edgeDeviceFx() {
        return new FanoutExchange(TOPIC_EDGE_DEVICE);
    }

    @Bean
    public Queue queueAlarmService() {
        return new Queue(QUEUE_ALARM_EDGE_DATA, true);
    }

    @Bean
    public DirectExchange alarmDx() {
        return new DirectExchange(TOPIC_ALARM);
    }

    @Bean
    public Queue queueAlarmConfig() {
        return new Queue(QUEUE_ALARM_CONFIG, true);
    }

    @Bean
    public Queue queueAlarmConfigDel() {
        return new Queue(QUEUE_ALARM_CONFIG_DEL, true);
    }

    @Bean
    public Queue queueAlarmEvent() {
        return new Queue(QUEUE_ALARM_EVENT, true);
    }

    @Bean
    public Binding bindingEdgeData() {
        return BindingBuilder.bind(queueAlarmService()).to(edgeDeviceFx());
    }

    @Bean
    public Binding bindingAlarmConfig() {
        return BindingBuilder.bind(queueAlarmConfig()).to(alarmDx()).with("config");
    }

    @Bean
    public Binding bindingAlarmEvent() {
        return BindingBuilder.bind(queueAlarmEvent()).to(alarmDx()).with("alarm");
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        return connectionFactory;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setConcurrentConsumers(concurrency);
        factory.setMaxConcurrentConsumers(maxConcurrency);
        return factory;
    }

    @Bean
    public MessageHandler eventResultHandler() {
        return new MessageHandler();
    }

    @Override
    public void configureRabbitListeners(
        RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(myHandlerMethodFactory());
    }

    @Bean
    public DefaultMessageHandlerMethodFactory myHandlerMethodFactory() {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(new MappingJackson2MessageConverter());
        return factory;
    }
}