package com.centralroot.alarmservice.config;

import com.centralroot.alarmservice.entities.UserAlarmConfig;
import com.centralroot.alarmservice.model.AlarmConditionFactory;
import com.centralroot.alarmservice.model.AlarmConditionManager;
import com.centralroot.alarmservice.model.AlarmConditionSubject;
import com.centralroot.alarmservice.repository.AlarmConfigRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AlarmSubjectConfig {

  @Autowired
  private AlarmConfigRepository alarmConfigRepository;

  @Bean
  public AlarmConditionSubject alarmConditionSubject(){
    AlarmConditionFactory conditionFactory = new AlarmConditionFactory();
    AlarmConditionSubject alarmConditionManager = new AlarmConditionManager();

    List<UserAlarmConfig> allAlarmConfig = alarmConfigRepository.findAll();
    System.out.println(allAlarmConfig);
    for (UserAlarmConfig config: allAlarmConfig){
      alarmConditionManager.register(conditionFactory.getAlarmCondition(config));
    }
    System.out.println("Condition size: "+alarmConditionManager.getAlarmConditions().size());
    return alarmConditionManager;
  }
}
