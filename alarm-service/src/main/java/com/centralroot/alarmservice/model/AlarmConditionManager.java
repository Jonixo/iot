package com.centralroot.alarmservice.model;

import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_EVENT;

import com.centralroot.alarmservice.messaging.MessageSender;
import com.centralroot.common.model.EdgeData;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AlarmConditionManager implements AlarmConditionSubject {

  @Autowired
  private MessageSender messageSender;

  private ArrayList<AlarmCondition> alarmConditions;

  public AlarmConditionManager(){
    alarmConditions = new ArrayList<>();
  }

  @Override
  public AlarmCondition findConditionByConfigId(String configId) {
    for (AlarmCondition alarmCondition: alarmConditions){
      if (alarmCondition.getConfigId().equals(configId)){
        return alarmCondition;
      }
    }
    return null;
  }

  @Override
  public void register(AlarmCondition alarmCondition) {
    if (alarmCondition != null && !alarmConditions.contains(alarmCondition)){
      alarmConditions.add(alarmCondition);
    }
  }

  @Override
  public void remove(AlarmCondition alarmCondition) {
    alarmConditions.remove(alarmCondition);
  }

  @Override
  public void remove(String configId) {
    AlarmCondition alarmCondition = findConditionByConfigId(configId);
    if (alarmCondition != null){
      remove(alarmCondition);
    }
  }

  @Override
  public void notifyObservers(EdgeData edgeData) {
    for (AlarmCondition condition: alarmConditions){
      if(condition.check(edgeData)){
        messageSender.sendMessage(QUEUE_ALARM_EVENT, condition.getAlarmEvent(edgeData));
      }
    }
  }

  @Override
  public ArrayList<AlarmCondition> getAlarmConditions() {
    return alarmConditions;
  }
}
