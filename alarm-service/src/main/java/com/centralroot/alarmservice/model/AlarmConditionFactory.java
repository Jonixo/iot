package com.centralroot.alarmservice.model;

import com.centralroot.alarmservice.entities.UserAlarmConfig;

public class AlarmConditionFactory {

  public AlarmCondition getAlarmCondition(UserAlarmConfig config){
    switch (config.getType().toLowerCase()){
      case "max":
        return new MaxCondition(config.getConfigId(), config.getLevel(), config.getDeviceId(),
            config.getSensorType(), config.getUsername(), config.getValue());
      default:
        return null;
    }
  }
}
