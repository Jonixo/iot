package com.centralroot.alarmservice.model;

import com.centralroot.common.model.AlarmInfo;
import com.centralroot.common.model.EdgeData;
import java.time.Instant;
import java.util.Objects;
import lombok.Getter;

@Getter
public abstract class AlarmCondition {

  String configId;
  String level;
  String deviceId;
  String sensorType;
  String username;
  ConditionState state;

  public AlarmCondition(String configId, String level, String deviceId, String sensorType,
      String username, ConditionState conditionState){
    this.configId = configId;
    this.level = level;
    this.deviceId = deviceId;
    this.sensorType = sensorType;
    this.username = username;
    this.state = conditionState;
  }

  public void setState(ConditionState conditionState){
    this.state = conditionState;
  }

  public boolean check(EdgeData edgeData){
    return state.raiseAlarm(this, edgeData);
  }

  public AlarmInfo getAlarmEvent(EdgeData edgeData){
    Instant instant = Instant.now();
    long timeStampMillis = instant.toEpochMilli();
    return new AlarmInfo(edgeData.getTime(), timeStampMillis, configId, level, deviceId,
        sensorType, edgeData.getValue(), username);
  }

  public abstract boolean isSatisfied(EdgeData edgeData);

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AlarmCondition that = (AlarmCondition) o;
    return configId.equals(that.configId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(configId);
  }
}
