package com.centralroot.alarmservice.model;

import com.centralroot.common.model.EdgeData;

public class AlarmState implements ConditionState {

  @Override
  public boolean raiseAlarm(AlarmCondition alarmCondition, EdgeData edgeData) {
    if (alarmCondition.isSatisfied(edgeData)) {
      System.out.println("Alarm raised already: " + edgeData.getSensorType() + ":" + edgeData.getValue());
    }
    return false;
  }
}
