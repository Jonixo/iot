package com.centralroot.alarmservice.model;

import com.centralroot.common.model.EdgeData;

public class NormalState implements ConditionState {

  @Override
  public boolean raiseAlarm(AlarmCondition alarmCondition, EdgeData edgeData) {
    if (alarmCondition.isSatisfied(edgeData)){
      //alarmCondition.sendAlarmEvent(edgeData);
      //alarmCondition.setState(new AlarmState());
      return true;
    }
    return false;
  }
}
