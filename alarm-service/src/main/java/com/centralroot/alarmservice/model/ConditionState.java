package com.centralroot.alarmservice.model;

import com.centralroot.common.model.EdgeData;
import lombok.AllArgsConstructor;

public interface ConditionState {
  boolean raiseAlarm(AlarmCondition alarmCondition, EdgeData edgeData);
}
