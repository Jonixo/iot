package com.centralroot.alarmservice.model;

import com.centralroot.common.model.EdgeData;
import java.util.ArrayList;

public interface AlarmConditionSubject {
  void register(AlarmCondition alarmCondition);
  void remove(AlarmCondition alarmCondition);
  void remove(String configId);
  void notifyObservers(EdgeData edgeData);
  ArrayList<AlarmCondition> getAlarmConditions();
  AlarmCondition findConditionByConfigId(String configId);
}
