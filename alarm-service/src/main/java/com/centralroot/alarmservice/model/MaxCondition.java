package com.centralroot.alarmservice.model;

import com.centralroot.common.model.EdgeData;
import lombok.Getter;

@Getter
public class MaxCondition extends AlarmCondition {
  private double maxValue;
  private String type = "max";

  public MaxCondition(String configId, String level, String deviceId, String sensorType, String username, double value) {
    super(configId, level, deviceId, sensorType, username, new NormalState());
    this.maxValue = value;
  }

  @Override
  public boolean isSatisfied(EdgeData edgeData) {
    return deviceId.equalsIgnoreCase(edgeData.getDeviceId())
        && sensorType.equalsIgnoreCase(edgeData.getSensorType())
        && maxValue < edgeData.getValue();
  }
}
