package com.centralroot.alarmservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "alarm_config")
@ToString
public class UserAlarmConfig {
  @Id
  private String configId;

  private String type;

  private String level;

  private String deviceId;

  private String sensorType;

  private String username;

  private double value;
}
