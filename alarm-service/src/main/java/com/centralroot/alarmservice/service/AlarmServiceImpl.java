package com.centralroot.alarmservice.service;

import com.centralroot.alarmservice.entities.UserAlarmConfig;
import com.centralroot.alarmservice.model.AlarmCondition;
import com.centralroot.alarmservice.model.AlarmConditionFactory;
import com.centralroot.alarmservice.model.AlarmConditionSubject;
import com.centralroot.alarmservice.repository.AlarmConfigRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AlarmServiceImpl implements AlarmService {

  @Autowired
  private AlarmConfigRepository alarmConfigRepository;

  @Autowired
  private AlarmConditionSubject alarmConditionSubject;

  private AlarmConditionFactory conditionFactory = new AlarmConditionFactory();

  @Override
  public void saveAlarmConfig(UserAlarmConfig alarmConfig) {
    UserAlarmConfig save = alarmConfigRepository.save(alarmConfig);
    AlarmCondition alarmCondition = conditionFactory.getAlarmCondition(save);
    alarmConditionSubject.register(alarmCondition);
  }

  @Override
  public List<UserAlarmConfig> findAllAlarmConfig() {
    return alarmConfigRepository.findAll();
  }

  @Override
  public void deleteConfig(String configId) {
    alarmConfigRepository.deleteByConfigId(configId);
  }
}
