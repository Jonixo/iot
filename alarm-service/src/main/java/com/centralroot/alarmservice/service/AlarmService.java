package com.centralroot.alarmservice.service;

import com.centralroot.alarmservice.entities.UserAlarmConfig;
import java.util.List;

public interface AlarmService {
  void saveAlarmConfig(UserAlarmConfig alarmConfig);
  List<UserAlarmConfig> findAllAlarmConfig();
  void deleteConfig(String configId);
}
