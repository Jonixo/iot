package com.centralroot.alarmservice.messaging;

import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_CONFIG;
import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_CONFIG_DEL;
import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_EDGE_DATA;

import com.centralroot.alarmservice.entities.UserAlarmConfig;
import com.centralroot.alarmservice.model.AlarmConditionSubject;
import com.centralroot.alarmservice.service.AlarmService;
import com.centralroot.common.model.AlarmConfigInfo;
import com.centralroot.common.model.EdgeData;
import com.centralroot.common.model.AlarmConfigId;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;


@Component
public class MessageHandler {

    @Autowired
    private AlarmConditionSubject alarmConditionSubject;

    @Autowired
    private AlarmService alarmService;

    @RabbitListener(queues=QUEUE_ALARM_EDGE_DATA)
    public void handleEdgeData(@Payload EdgeData edgeData) {
        alarmConditionSubject.notifyObservers(edgeData);
    }

    @RabbitListener(queues=QUEUE_ALARM_CONFIG)
    public void handleConfig(@Payload AlarmConfigInfo alarmConfig) {
        System.out.println("Alarm config msg: "+alarmConfig+":"+alarmConditionSubject.getAlarmConditions().size());
        alarmService.saveAlarmConfig(UserAlarmConfig.builder()
            .configId(alarmConfig.getConfigId())
            .deviceId(alarmConfig.getDeviceId())
            .level(alarmConfig.getLevel())
            .sensorType(alarmConfig.getSensorType())
            .type(alarmConfig.getType())
            .username(alarmConfig.getUsername())
            .value(alarmConfig.getValue()).build());
        System.out.println("New condition sizes: "+alarmConditionSubject.getAlarmConditions().size());
    }

    @RabbitListener(queues=QUEUE_ALARM_CONFIG_DEL)
    public void handleConfigDelete(@Payload AlarmConfigId alarmConfig) {
        System.out.println("Alarm config delete msg: "+alarmConfig);
        alarmConditionSubject.remove(alarmConfig.getConfigId());
        alarmService.deleteConfig(alarmConfig.getConfigId());
    }
}
