package com.centralroot.backendrestservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Entity
@Table(name = "users")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = 7899617862526921765L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
    @SequenceGenerator(name="user_generator", sequenceName = "user_seq", initialValue = 10)
    @Column(name = "id", nullable = false, updatable = false)
    @JsonIgnore
    private Long id;

    @NotEmpty(message = "Username cannot be empty!")
    @Size(min = 6, max = 15, message = "Username must be between 6-15 characters.")
    @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Username can contain only letters and digits.")
    @Column(name = "username", nullable = false, updatable = false, unique = true)
    private String username;

    @NotEmpty(message = "Password cannot be empty!")
    @Size(min = 8, max = 60, message = "Password length must be minimum 8 characters.")
    @Pattern(
        // Plain password is validated in controller. After passed validation, password is encoded
        // with Bcrypt in UserService. When persisting this entity, JPA will again bean-validate
        // the entity before saving it to database (pre-persist). Therefore, validation methods
        // must be compatible with Bcrypt format. If this validation methods or hashing are required
        // to change, consider this issue or write custom validator.
        regexp = "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!#$&()/*+-.=?@_{}~])[A-Za-z0-9!#$&()/*+-.=?@_{}~]{4,}$",
        message = "Password must have at least one uppercase letter, one lowercase letter, " +
            "one digit and one special character (!#$&()/*+-.=?@_{}~).")
    @Column(name = "password", nullable = false)
    @JsonIgnore
    private String password;

    @NotEmpty(message = "Name cannot be empty!")
    @Size(min = 1, max = 50, message = "Name must be between 1-50 characters.")
    @Pattern(regexp = "^[a-zA-Z\\s]*$", message = "Name can contain only letters.")
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotEmpty(message = "Surname cannot be empty!")
    @Size(min = 1, max = 50, message = "Surname must be between 1-50 characters.")
    @Pattern(regexp = "^[a-zA-Z\\s]*$", message = "Surname can contain only letters.")
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @ManyToMany()
    @JoinTable(
        name = "users_device",
        joinColumns = @JoinColumn(
            name = "user_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(
            name = "device_id", referencedColumnName = "id"))
    @JsonIgnore
    private Set<EdgeDevice> edgedevices;

    @ManyToMany()
    @JoinTable(
            name = "users_role",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    @JsonIgnore
    private Set<Role> roles;

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY,
        cascade = CascadeType.ALL)
    private UserSetting usersettings;

    @OneToMany(mappedBy="user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CustomAlarmCondition> customAlarmConditions;

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
