package com.centralroot.backendrestservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "alarm_conditions")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomAlarmCondition implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "alarm_conditions_generator")
  @SequenceGenerator(name="alarm_conditions_generator", sequenceName = "alarm_conditions_seq", initialValue = 10)
  @Column(name = "id", nullable = false, updatable = false)
  @JsonIgnore
  private Long id;

  @Column(name = "config_id", nullable = false, updatable = false, unique = true)
  private String configId;

  @NotEmpty(message = "Type cannot be empty")
  @Column(name = "type", nullable = false, updatable = false)
  private String type;

  @NotEmpty(message = "Description cannot be empty")
  @Column(name = "description", nullable = false)
  private String description;

  @NotEmpty(message = "Level cannot be empty")
  @Column(name = "level", nullable = false)
  private String level;

  @NotEmpty(message = "Device cannot be empty")
  @Column(name = "device_id", nullable = false, updatable = false)
  private String deviceId;

  @NotEmpty(message = "Sensor type cannot be empty")
  @Column(name = "sensor_type", nullable = false, updatable = false)
  private String sensorType;

  @Column(name = "value", nullable = false, updatable = false,
      columnDefinition="Decimal(10,2)")
  private double value;

  @ManyToOne
  @JoinColumn(name="username", referencedColumnName = "username", nullable=false)
  @JsonIgnore
  private User user;

  @OneToMany(mappedBy="customAlarmCondition", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JsonIgnore
  private Set<Alarm> alarms;

  @Column(name = "active", nullable = false)
  private boolean active;
}
