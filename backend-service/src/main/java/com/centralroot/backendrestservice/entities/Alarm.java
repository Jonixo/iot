package com.centralroot.backendrestservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "alarms")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Alarm {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "alarm_generator")
  @SequenceGenerator(name="alarm_generator", sequenceName = "alarm_seq", initialValue = 10)
  @Column(name = "id", nullable = false, updatable = false)
  @JsonIgnore
  private Long id;

  @NotEmpty
  @Column(name = "alarm_id", nullable = false, updatable = false, unique = true)
  private String alarmId;

  @Column(name = "timestamp", nullable = false, updatable = false)
  private long alarmTimestamp;

  @Column(name = "data_timestamp", nullable = false, updatable = false)
  private long dataTimestamp;

  @Column(name = "value", nullable = false, updatable = false,
      columnDefinition="Decimal(10,2)")
  private double value;

  @Column(name = "checked", nullable = false)
  private boolean checked;

  @ManyToOne
  @JoinColumn(name="config_id", referencedColumnName = "config_id", nullable=false)
  @JsonIgnore
  private CustomAlarmCondition customAlarmCondition;
}
