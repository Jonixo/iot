package com.centralroot.backendrestservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "usersettings")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserSetting implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usersettings_generator")
  @SequenceGenerator(name="usersettings_generator", sequenceName = "usersettings_seq", initialValue = 10)
  @Column(name = "id", nullable = false, updatable = false)
  @JsonIgnore
  private Long id;

  @OneToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "user_id", referencedColumnName = "id")
  @JsonIgnore
  private User user;

  @NotBlank(message = "Device Id cannot be blank!")
  @Size(min = 12, max = 32, message = "Device Id length must be between 12-32.")
  @Pattern(regexp = "^[a-zA-Z0-9-]*$", message = "Device Id can contain only letters, digits and dash sign.")
  @Column(name = "device_id")
  private String deviceId;

  @NotBlank(message = "Device Tag cannot be blank!")
  @Size(max = 16, message = "Device Tag length cannot be bigger than 16.")
  @Pattern(regexp = "^[a-zA-Z0-9-]*$", message = "Device Tag can contain only letters, digits and dash sign.")
  @Column(name = "device_tag")
  private String deviceTag;

  @NotBlank(message = "Sensor name cannot be blank.")
  @Pattern(regexp = "^[a-zA-Z0-9-]*$", message = "Sensor name can contain only letters, digits and dash sign.")
  @Size(max = 16, message = "Sensor name length cannot be bigger than 16.")
  @Column(name = "sensor_type")
  private String sensorType;
}
