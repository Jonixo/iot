package com.centralroot.backendrestservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "device_credentials")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceCredentials {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "device_credentials_generator")
  @SequenceGenerator(name="device_credentials_generator", sequenceName = "device_credentials_seq", initialValue = 10)
  @Column(name = "id", nullable = false, updatable = false)
  @JsonIgnore
  private Long id;

  @OneToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "device_id", referencedColumnName = "id")
  @JsonIgnore
  private EdgeDevice edgeDevice;

  @NotEmpty
  @Column(name = "protocol", nullable = false)
  private String protocol;

  @NotEmpty
  @Column(name = "credential", nullable = false)
  private String credential;
}
