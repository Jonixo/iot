package com.centralroot.backendrestservice.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

@Entity
@Table(name = "edgedevices")
@TypeDefs({
    @TypeDef(
        typeClass = StringArrayType.class,
        defaultForType = String[].class
    )
})
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EdgeDevice {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, updatable = false)
    @JsonIgnore
    private long id;

    @Column(name = "device_id", nullable = false, updatable = false, unique = true)
    private String deviceId;

    @NotEmpty
    @Column(name = "tag", nullable = false)
    private String tag;

    @NotEmpty
    @Column(name = "description", nullable = false)
    private String description;

    @Column(
        name = "sensor_names",
        columnDefinition = "text[]"
    )
    private String[] sensorNames;

    @ManyToMany(mappedBy = "edgedevices", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<User> users;

    @OneToOne(mappedBy = "edgeDevice", fetch = FetchType.LAZY,
        cascade = CascadeType.ALL)
    private DeviceCredentials deviceCredentials;

    public String getSensors() {
        StringBuilder sb = new StringBuilder();
        int sensorSize = sensorNames.length;
        for (int i = 0; i < sensorSize; i++){
            sb.append(sensorNames[i]);
            if (i != sensorSize - 1){
                sb.append(", ");
            }
        }
        return sb.toString();
    }
}
