package com.centralroot.backendrestservice.repository;


import com.centralroot.backendrestservice.entities.User;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    @Query(value = "select u from User u join u.roles r where r.name in :roles")
    List<User> findByRolesIn(@Param("roles") Collection<String> roles);

    @Query(value = "select u from User u join u.roles r where r.name not in :roles")
    List<User> findByRolesNotIn(@Param("roles") Collection<String> roles);

    int deleteByUsername(String username);

    @Query(value = "select u from User u join u.edgedevices e where e.deviceId like :deviceId")
    List<User> findUsersByDeviceId(@Param("deviceId") String deviceId);
}