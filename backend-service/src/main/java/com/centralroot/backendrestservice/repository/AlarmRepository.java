package com.centralroot.backendrestservice.repository;

import com.centralroot.backendrestservice.entities.Alarm;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlarmRepository extends JpaRepository<Alarm, Long> {
  List<Alarm> findTop10ByCustomAlarmConditionUserUsernameAndCustomAlarmConditionDeviceIdInOrderByAlarmTimestampDesc(String username, List<String> deviceIds);
  int countAlarmByCustomAlarmConditionUserUsernameAndCheckedFalseAndCustomAlarmConditionDeviceIdIn(String username, List<String> deviceIds);
  List<Alarm> findAllByCustomAlarmConditionUserUsernameAndCustomAlarmConditionDeviceIdInOrderByAlarmTimestampDesc(String username, List<String> deviceIds);
  Alarm findByAlarmIdAndCustomAlarmConditionUserUsernameAndCustomAlarmConditionDeviceIdIn(String alarmId, String username, List<String> deviceIds);
}
