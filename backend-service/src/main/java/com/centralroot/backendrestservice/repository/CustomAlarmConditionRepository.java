package com.centralroot.backendrestservice.repository;

import com.centralroot.backendrestservice.entities.CustomAlarmCondition;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomAlarmConditionRepository extends JpaRepository<CustomAlarmCondition, Long> {

  @Query(value = "select a from CustomAlarmCondition as a join a.user u where u.username like :username")
  List<CustomAlarmCondition> findByUsername(@Param("username") String username);

  int deleteByConfigId(String configId);

  CustomAlarmCondition findByConfigId(String configId);

  @Query(value = "select a from CustomAlarmCondition as a join a.user u where u.username like :username and a.active = true")
  List<CustomAlarmCondition> findByUsernameAndActiveTrue(@Param("username") String username);

  List<CustomAlarmCondition> findByDeviceIdAndUserUsernameAndActiveTrue(String deviceId, String username);
}
