package com.centralroot.backendrestservice.repository;

import com.centralroot.backendrestservice.entities.EdgeDevice;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DeviceRepository extends JpaRepository<EdgeDevice, Long> {

  @Query(value = "select e from EdgeDevice e join e.users u where u.username in :usernames")
  List<EdgeDevice> findByUsernames(@Param("usernames") Collection<String> usernames);

  @Query(value = "select case when count(e)> 0 then true else false end from EdgeDevice e "
      + "join e.users u where u.username like :username and e.deviceId like :deviceId")
  boolean existsByDeviceIdAndUsername(@Param("username") String username, @Param("deviceId") String deviceId);

  EdgeDevice findByDeviceId(String deviceId);
}
