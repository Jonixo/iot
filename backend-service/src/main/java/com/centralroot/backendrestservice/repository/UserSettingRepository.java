package com.centralroot.backendrestservice.repository;

import com.centralroot.backendrestservice.entities.UserSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserSettingRepository extends JpaRepository<UserSetting, Long> {

  @Query(value = "select us from UserSetting us inner join us.user u where u.username like concat('%',:username,'%')")
  UserSetting findSettingsByUsername(@Param("username") String username);

  @Modifying
  @Query(value = "delete from UserSetting us where us in (select uss from UserSetting uss join uss.user u where u.username = :username)")
  int deleteByUsername(@Param("username") String username);
}
