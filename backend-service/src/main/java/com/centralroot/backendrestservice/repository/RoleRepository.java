package com.centralroot.backendrestservice.repository;


import com.centralroot.backendrestservice.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RoleRepository extends JpaRepository<Role, Long> {

  @Query(value = "select r from Role r where r.name like concat('%',:role,'%')")
  Role findByRoleName(@Param("role") String role);
}
