package com.centralroot.backendrestservice.messaging;

import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_EVENT;
import static com.centralroot.common.messaging.Queues.QUEUE_BACKEND_EDGE_DATA;

import com.centralroot.backendrestservice.entities.UserSetting;
import com.centralroot.backendrestservice.service.AlarmService;
import com.centralroot.backendrestservice.service.UserService;
import com.centralroot.common.model.AlarmInfo;
import com.centralroot.common.model.EdgeData;
import java.util.List;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;


@Component
public class MessageHandler {
    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private UserService userService;

    @Autowired
    private AlarmService alarmService;

    @RabbitListener(queues=QUEUE_BACKEND_EDGE_DATA)
    public void handleMessage(@Payload EdgeData edgeData) {
        List<String> loggedinUsers = userService.getAllLoggedinUsers();
        for (String username : loggedinUsers) {
            UserSetting userSetting = userService.findUserSettingsByUsername(username);
            if (null != userSetting && edgeData.getDeviceId().equals(userSetting.getDeviceId())
                && edgeData.getSensorType().equals(userSetting.getSensorType())){
                template.convertAndSendToUser(username,"/queue/realtime", edgeData);
            }
        }
    }

    @RabbitListener(queues=QUEUE_ALARM_EVENT)
    public void handleAlarmEvent(@Payload AlarmInfo alarm) {
        template.convertAndSendToUser(alarm.getUsername(),"/queue/alarm", alarm);
        alarmService.saveAlarm(alarm);
    }
}
