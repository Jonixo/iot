package com.centralroot.backendrestservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceSystemInfo {
  private String cpuUsage;
  private String memoryUsage;
  private String diskUsage;
}
