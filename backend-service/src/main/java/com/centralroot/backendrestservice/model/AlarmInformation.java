package com.centralroot.backendrestservice.model;


import com.fasterxml.jackson.annotation.JsonRootName;
import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
@Builder
@JsonRootName(value="AlarmInformation")
public class AlarmInformation {
  private String alarmId;
  private String level;
  private String type;
  private double value;
  private double threshold;
  private String deviceId;
  private String sensorType;
  private ZonedDateTime timestamp;
  private boolean checked;
}
