package com.centralroot.backendrestservice.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DataTrendOptions {
  @Min(value = 0, message = "Start Date must be bigger than zero.")
  private long startDate;

  @Min(value = 0, message = "End Date must be bigger than zero.")
  private long endDate;

  @NotBlank(message = "Device Id cannot be blank.")
  @Pattern(regexp = "^[a-zA-Z0-9-]*$", message = "Device Id can contain only letters, digits and dash sign.")
  @Size(min = 12, max = 32, message = "Device Id size must be between 12-32.")
  private String deviceId;

  @NotBlank(message = "Sensor name cannot be blank.")
  @Pattern(regexp = "^[a-zA-Z0-9-]*$", message = "Sensor name can contain only letters, digits and dash sign.")
  @Size(max = 16, message = "Sensor name length cannot be bigger than 16.")
  private String sensorName;
}
