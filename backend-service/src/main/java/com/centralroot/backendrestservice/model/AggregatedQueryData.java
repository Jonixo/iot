package com.centralroot.backendrestservice.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.time.ZonedDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Builder
@NoArgsConstructor
@Getter
@Setter
@JsonRootName(value="AggregatedQueryData")
public class AggregatedQueryData {

    private ZonedDateTime time;
    private double average;
    private double total;
    private double min;
    private double max;

    public AggregatedQueryData(ZonedDateTime time, double average, double total, double min,
        double max) {
        this.time = time;
        if (Double.isInfinite(average)){
            this.average = this.total = this.min = this.max = 0;
        } else {
            this.average = average;
            this.total = total;
            this.min = min;
            this.max = max;
        }
    }
}