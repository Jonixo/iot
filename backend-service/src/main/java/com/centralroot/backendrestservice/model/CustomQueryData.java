package com.centralroot.backendrestservice.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@Setter
@Builder
@NoArgsConstructor
@JsonRootName(value="CustomQueryData")
public class CustomQueryData {
    private long time;
    private double value;
}
