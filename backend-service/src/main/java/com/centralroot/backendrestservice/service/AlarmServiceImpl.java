package com.centralroot.backendrestservice.service;

import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_CONFIG;
import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_CONFIG_DEL;

import com.centralroot.backendrestservice.entities.Alarm;
import com.centralroot.backendrestservice.entities.CustomAlarmCondition;
import com.centralroot.backendrestservice.entities.EdgeDevice;
import com.centralroot.backendrestservice.messaging.MessageSender;
import com.centralroot.backendrestservice.model.AlarmInformation;
import com.centralroot.backendrestservice.repository.AlarmRepository;
import com.centralroot.backendrestservice.repository.CustomAlarmConditionRepository;
import com.centralroot.common.model.AlarmConfigId;
import com.centralroot.common.model.AlarmConfigInfo;
import com.centralroot.common.model.AlarmInfo;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AlarmServiceImpl implements AlarmService {

  @Autowired
  private CustomAlarmConditionRepository alarmConditionRepository;

  @Autowired
  private AlarmRepository alarmRepository;

  @Autowired
  private UserService userService;

  @Autowired
  private DeviceService deviceService;

  @Autowired
  private MessageSender messageSender;

  private List<String> getUserDevicesIds(String username){
    List<EdgeDevice> userDevices = deviceService.getUserDevices(username);
    return userDevices.stream().map(EdgeDevice::getDeviceId).collect(Collectors.toList());
  }

  @Override
  public List<CustomAlarmCondition> findAlarmConditionsByUsername(String username) {
    List<String> deviceIds = getUserDevicesIds(username);

    List<CustomAlarmCondition> alarmConditions = alarmConditionRepository
        .findByUsernameAndActiveTrue(username);

    return alarmConditions.stream()
        .filter(condition -> deviceIds.contains(condition.getDeviceId()))
        .collect(Collectors.toList());
  }

  @Override
  public List<CustomAlarmCondition> findAlarmConditionsByUsernameAndDeviceId(String username,
      String deviceId) {
    return alarmConditionRepository.findByDeviceIdAndUserUsernameAndActiveTrue(deviceId, username);
  }

  @Override
  @Transactional
  public CustomAlarmCondition deleteAlarmConfigByConfigId(String configId) {
    CustomAlarmCondition alarmCondition = alarmConditionRepository.findByConfigId(configId);
    alarmCondition.setActive(false);
    messageSender.sendMessage(QUEUE_ALARM_CONFIG_DEL, new AlarmConfigId(configId));
    return alarmConditionRepository.save(alarmCondition);
  }

  private String createAlarmId(String configId){
    return configId + "-" + RandomStringUtils.randomAlphanumeric(6);
  }

  @Override
  @Transactional
  public CustomAlarmCondition saveAlarm(AlarmInfo alarmInfo) {
    CustomAlarmCondition alarmCondition = alarmConditionRepository.findByConfigId(alarmInfo.getConfigId());
    if (alarmCondition == null) {
      return null;
    }
    Alarm alarm = Alarm.builder()
        .alarmId(createAlarmId(alarmInfo.getConfigId()))
        .alarmTimestamp(alarmInfo.getAlarmTimestamp())
        .customAlarmCondition(alarmCondition)
        .checked(false)
        .dataTimestamp(alarmInfo.getDataTimestamp())
        .value(alarmInfo.getValue()).build();
    alarmCondition.getAlarms().add(alarm);

    return alarmConditionRepository.save(alarmCondition);
  }

  @Override
  public CustomAlarmCondition saveAlarmCondition(CustomAlarmCondition condition, String username) {
    condition.setActive(true);
    condition.setUser(userService.findByUsername(username));
    condition.setConfigId(RandomStringUtils.randomAlphanumeric(6));
    CustomAlarmCondition save = alarmConditionRepository.save(condition);
    AlarmConfigInfo configInfo = AlarmConfigInfo.builder()
        .type(condition.getType())
        .value(condition.getValue())
        .username(condition.getUser().getUsername())
        .sensorType(condition.getSensorType())
        .deviceId(condition.getDeviceId())
        .level(condition.getLevel())
        .configId(condition.getConfigId()).build();
    messageSender.sendMessage(QUEUE_ALARM_CONFIG, configInfo);
    return save;
  }

  @Override
  public CustomAlarmCondition saveAlarmCondition(CustomAlarmCondition condition) {
    return alarmConditionRepository.save(condition);
  }

  @Override
  public List<AlarmInformation> getAllAlarms(String username) {
    List<String> deviceIds = getUserDevicesIds(username);

    List<AlarmInformation> alarmInformation = new ArrayList<>();
    List<Alarm> alarms = alarmRepository
        .findAllByCustomAlarmConditionUserUsernameAndCustomAlarmConditionDeviceIdInOrderByAlarmTimestampDesc(username, deviceIds);
    for (Alarm alarm: alarms){
      alarmInformation.add(AlarmInformation.builder()
          .alarmId(alarm.getAlarmId())
          .deviceId(alarm.getCustomAlarmCondition().getDeviceId())
          .level(alarm.getCustomAlarmCondition().getLevel())
          .sensorType(alarm.getCustomAlarmCondition().getSensorType())
          .timestamp(ZonedDateTime.ofInstant(Instant.ofEpochMilli(alarm.getAlarmTimestamp()), ZoneId.of("Asia/Istanbul")))
          .type(alarm.getCustomAlarmCondition().getType())
          .value(alarm.getValue())
          .threshold(alarm.getCustomAlarmCondition().getValue())
          .checked(alarm.isChecked()).build());
    }
    return alarmInformation;
  }

  @Override
  public List<AlarmInfo> getLastTenAlarms(String username) {
    List<String> deviceIds = getUserDevicesIds(username);

    List<Alarm> alarms = alarmRepository.findTop10ByCustomAlarmConditionUserUsernameAndCustomAlarmConditionDeviceIdInOrderByAlarmTimestampDesc(username, deviceIds);
    List<AlarmInfo> alarmInfos = new ArrayList<>();
    for (Alarm alarm: alarms){
      alarmInfos.add(AlarmInfo.builder()
          .sensorType(alarm.getCustomAlarmCondition().getSensorType())
          .value(alarm.getValue())
          .level(alarm.getCustomAlarmCondition().getLevel())
          .alarmTimestamp(alarm.getAlarmTimestamp()).build());
    }
    return alarmInfos;
  }

  @Override
  public int getNumberOfUnreadAlarms(String username) {
    List<String> deviceIds = getUserDevicesIds(username);
    return alarmRepository.countAlarmByCustomAlarmConditionUserUsernameAndCheckedFalseAndCustomAlarmConditionDeviceIdIn(username, deviceIds);
  }

  @Override
  @Transactional
  public Alarm setAlarmChecked(String alarmId, String username) {
    List<String> deviceIds = getUserDevicesIds(username);
    Alarm alarm = alarmRepository.findByAlarmIdAndCustomAlarmConditionUserUsernameAndCustomAlarmConditionDeviceIdIn(alarmId, username, deviceIds);
    if (alarm == null){
      return new Alarm();
    }
    alarm.setChecked(true);
    return alarmRepository.save(alarm);
  }
}
