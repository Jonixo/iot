package com.centralroot.backendrestservice.service;

import com.centralroot.backendrestservice.model.AggregatedQueryData;
import com.centralroot.backendrestservice.model.DataTrendOptions;
import java.security.Principal;
import java.util.LinkedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataServiceImpl implements DataService {

  @Autowired
  private ElasticSearchQueryService queryService;

  @Autowired
  private DeviceService deviceService;

  private boolean validateUser(DataTrendOptions options, Principal user){
    return deviceService.existsByDeviceIdAndUsername(user.getName(), options.getDeviceId());
  }

  @Override
  public LinkedList<AggregatedQueryData> getAggregatedData(DataTrendOptions options, Principal user) {
    if (validateUser(options, user)){
      return queryService.getAggregatedData(options, 100);
    }
    return new LinkedList<>();
  }
}
