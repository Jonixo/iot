package com.centralroot.backendrestservice.service;

import com.centralroot.backendrestservice.model.DeviceSystemInfo;
import com.centralroot.backendrestservice.entities.EdgeDevice;
import java.security.Principal;
import java.util.Collection;
import java.util.List;

public interface DeviceService {
  List<EdgeDevice> findByUsernames(Collection<String> usernames);

  boolean existsByDeviceIdAndUsername(String username, String deviceId);

  List<EdgeDevice> getUserDevices(String username);

  List<EdgeDevice> getAllDevices();

  DeviceSystemInfo getDeviceSystemInfo(String deviceId, Principal user);
}
