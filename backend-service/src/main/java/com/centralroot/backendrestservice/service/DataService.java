package com.centralroot.backendrestservice.service;

import com.centralroot.backendrestservice.model.AggregatedQueryData;
import com.centralroot.backendrestservice.model.DataTrendOptions;
import java.security.Principal;
import java.util.LinkedList;

public interface DataService {
  LinkedList<AggregatedQueryData> getAggregatedData(DataTrendOptions options, Principal user);
}
