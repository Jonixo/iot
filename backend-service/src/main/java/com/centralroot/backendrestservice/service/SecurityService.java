package com.centralroot.backendrestservice.service;

public interface SecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
