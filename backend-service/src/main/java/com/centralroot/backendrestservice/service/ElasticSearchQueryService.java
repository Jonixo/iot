package com.centralroot.backendrestservice.service;

import com.centralroot.backendrestservice.model.AggregatedQueryData;
import com.centralroot.backendrestservice.model.CustomQueryData;

import com.centralroot.backendrestservice.model.DataTrendOptions;
import java.util.LinkedList;

public interface ElasticSearchQueryService {

    LinkedList<CustomQueryData> getSearchData(DataTrendOptions options);

    LinkedList<AggregatedQueryData> getAggregatedData(DataTrendOptions options, int numOfGroupings);
}
