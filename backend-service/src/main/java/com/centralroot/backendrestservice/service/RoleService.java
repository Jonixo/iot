package com.centralroot.backendrestservice.service;

import com.centralroot.backendrestservice.entities.Role;
import org.springframework.data.repository.query.Param;

public interface RoleService {
  Role findByRoleName(@Param("roles") String role);
}
