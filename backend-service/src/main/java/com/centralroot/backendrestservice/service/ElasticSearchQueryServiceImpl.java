package com.centralroot.backendrestservice.service;


import com.centralroot.backendrestservice.model.AggregatedQueryData;
import com.centralroot.backendrestservice.model.CustomQueryData;
import com.centralroot.backendrestservice.model.DataTrendOptions;
import java.time.ZonedDateTime;
import java.util.List;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.Max;
import org.elasticsearch.search.aggregations.metrics.Min;
import org.elasticsearch.search.aggregations.metrics.Sum;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.json.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;

@Service
public class ElasticSearchQueryServiceImpl implements ElasticSearchQueryService {

    @Autowired
    private RestHighLevelClient client;

    private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchQueryServiceImpl.class);

    @Autowired
    public ElasticSearchQueryServiceImpl(RestHighLevelClient client) {
        this.client = client;
    }

    public  LinkedList<CustomQueryData> getSearchData(DataTrendOptions options) {

        //range query for time
        QueryBuilder dateRangeQuery = QueryBuilders
                .rangeQuery("time")
                .from(options.getStartDate())
                .to(options.getEndDate());


        //term query for sensorType
        QueryBuilder sensorTypeTermsQuery = QueryBuilders.boolQuery()
                .must(QueryBuilders.termsQuery("sensorType", options.getSensorName().toLowerCase()));

        //term query for deviceId
        QueryBuilder deviceIdTermsQuery = QueryBuilders.boolQuery()
                .must(QueryBuilders.termsQuery("deviceId", options.getDeviceId()));

        //combining 3 querries
        QueryBuilder qb = QueryBuilders
                .boolQuery()
                .must(dateRangeQuery)
                .must(sensorTypeTermsQuery)
                .must(deviceIdTermsQuery);


        //creating search request
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(qb);
        //first 10k results
        searchSourceBuilder.from(0);
        searchSourceBuilder.size(10000);
        searchRequest.source(searchSourceBuilder);

        //creating linked list with CustomQueryResult (contains long time, double value)
        LinkedList<CustomQueryData> queryResult= new LinkedList<>();

        try {
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();

            //for loop for every hit, filtering json and adding results to linkedlist
            for (SearchHit hit: hits) {
                JSONObject json = new JSONObject(hit);
                JSONObject data = json.getJSONObject("sourceAsMap");
                CustomQueryData customQueryData = new CustomQueryData(Long.parseLong(data.get("time").toString()),Double.parseDouble(data.get("value").toString()));
                queryResult.add(customQueryData);
            }
        } catch (IOException e) {
            LOG.info(String.format("Search response exception: %s", e));
        }
        return queryResult;
    }

    //Es Aggregated Query Function
    public LinkedList<AggregatedQueryData> getAggregatedData(DataTrendOptions options, int numOfGroupings){

        //range query for time
        QueryBuilder dateRangeQuery = QueryBuilders
                .rangeQuery("time")
                .from(options.getStartDate())
                .to(options.getEndDate());


        //term query for sensorType
        QueryBuilder sensorTypeTermsQuery = QueryBuilders.boolQuery()
                .must(QueryBuilders.termsQuery("sensorType", options.getSensorName().toLowerCase()));

        //term query for deviceId
        QueryBuilder deviceIdTermsQuery = QueryBuilders.boolQuery()
                .must(QueryBuilders.termsQuery("deviceId", options.getDeviceId()));

        //combining 3 querries
        QueryBuilder qb = QueryBuilders
                .boolQuery()
                .must(dateRangeQuery)
                .must(sensorTypeTermsQuery)
                .must(deviceIdTermsQuery);



        //creating search request
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(qb);

        // Linked list to return
        LinkedList<AggregatedQueryData> queryResult= new LinkedList<>();

        // calculate time to aggregate
        // numOfGroupings-1 because this is the cut amount, if you cut something into 2 you get 3 pieces
        long timeInterval = ((options.getEndDate()/1000L)-(options.getStartDate()/1000L))/(numOfGroupings-1);

        // DateHistogramInterval cannot be zero or less
        if (timeInterval > 0){
            AggregationBuilder dateAggregation=
                AggregationBuilders.dateHistogram("date_aggregation")
                    .field("time")
                    .fixedInterval(DateHistogramInterval.seconds((int) timeInterval))
                    .subAggregation(AggregationBuilders.max("maxValue")
                        .field("value"))
                    .subAggregation(AggregationBuilders.min("minValue")
                        .field("value"))
                    .subAggregation(AggregationBuilders.avg("avgValue")
                        .field("value"))
                    .subAggregation(AggregationBuilders.sum("totalValue")
                        .field("value"));

            searchSourceBuilder.aggregation(dateAggregation);
            searchRequest.source(searchSourceBuilder);
            List<? extends Histogram.Bucket> buckets = new LinkedList<>();

            try {
                SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
                //Getting Parent Aggregation
                Histogram agg = searchResponse.getAggregations().get("date_aggregation");
                buckets = agg.getBuckets();
            } catch (IOException e) {
                LOG.info(String.format("Search response exception: %s", e));
            }
            for (Histogram.Bucket entry : buckets) {
                //getting child aggregations
                Min min_agg = entry.getAggregations().get("minValue");
                Max max_agg = entry.getAggregations().get("maxValue");
                Avg avg_agg = entry.getAggregations().get("avgValue");
                Sum sum_agg = entry.getAggregations().get("totalValue");
                queryResult.add(new AggregatedQueryData((ZonedDateTime) entry.getKey(),avg_agg.getValue(),sum_agg.getValue(),min_agg.getValue(),max_agg.getValue()));
            }
        }
        return queryResult;
    }
}
