package com.centralroot.backendrestservice.service;

import com.centralroot.backendrestservice.entities.Role;
import com.centralroot.backendrestservice.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
  @Autowired
  private RoleRepository roleRepository;

  @Override
  public Role findByRoleName(String role) {
    return roleRepository.findByRoleName(role);
  }
}
