package com.centralroot.backendrestservice.service;


import com.centralroot.backendrestservice.entities.User;

import com.centralroot.backendrestservice.entities.UserSetting;
import java.util.Collection;
import java.util.List;

public interface UserService {
    void save(User user);

    User findByUsername(String username);

    List<User> findAll();

    List<User> findNormalUsers(Collection<String> roles);

    int deleteNormalUserByUsername(String username);

    List<String> getAllLoggedinUsers();

    UserSetting findUserSettingsByUsername(String username);

    UserSetting saveUserSetting(UserSetting userSetting);

    List<User> findUsersByDeviceId(String deviceId);

    void assignDeviceToUser(String deviceId, List<String> usernames);

    int deleteSettingsByUsername(String username);

    void evictSingleUserSetting(String username);
}
