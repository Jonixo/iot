package com.centralroot.backendrestservice.service;

import com.centralroot.backendrestservice.model.DeviceSystemInfo;
import com.centralroot.backendrestservice.entities.EdgeDevice;
import com.centralroot.backendrestservice.repository.DeviceRepository;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeviceServiceImpl implements DeviceService{
  @Autowired
  private DeviceRepository deviceRepository;

  @Override
  public List<EdgeDevice> findByUsernames(Collection<String> usernames){
    return deviceRepository.findByUsernames(usernames);
  }

  @Override
  public boolean existsByDeviceIdAndUsername(String username, String deviceId) {
    return deviceRepository.existsByDeviceIdAndUsername(username, deviceId);
  }

  @Override
  public List<EdgeDevice> getUserDevices(String username) {
    return this.findByUsernames(Collections.singletonList(username));
  }

  @Override
  public List<EdgeDevice> getAllDevices() {
    return deviceRepository.findAll();
  }

  @Override
  public DeviceSystemInfo getDeviceSystemInfo(String deviceId, Principal user) {
    if (deviceId != null && existsByDeviceIdAndUsername(user.getName(), deviceId)){

    }
    return null;
  }
}
