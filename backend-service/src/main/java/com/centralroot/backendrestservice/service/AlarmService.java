package com.centralroot.backendrestservice.service;

import com.centralroot.backendrestservice.entities.Alarm;
import com.centralroot.backendrestservice.entities.CustomAlarmCondition;
import com.centralroot.backendrestservice.model.AlarmInformation;
import com.centralroot.common.model.AlarmInfo;
import java.util.List;

public interface AlarmService {
  List<CustomAlarmCondition> findAlarmConditionsByUsername(String username);
  List<CustomAlarmCondition> findAlarmConditionsByUsernameAndDeviceId(String username, String deviceId);
  CustomAlarmCondition deleteAlarmConfigByConfigId(String configId);
  CustomAlarmCondition saveAlarm(AlarmInfo alarmInfo);
  CustomAlarmCondition saveAlarmCondition(CustomAlarmCondition customAlarmCondition, String username);
  CustomAlarmCondition saveAlarmCondition(CustomAlarmCondition customAlarmCondition);
  List<AlarmInformation> getAllAlarms(String username);
  List<AlarmInfo> getLastTenAlarms(String username);
  int getNumberOfUnreadAlarms(String username);
  Alarm setAlarmChecked(String alarmId, String username);
}
