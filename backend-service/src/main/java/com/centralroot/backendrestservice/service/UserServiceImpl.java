package com.centralroot.backendrestservice.service;

import static com.centralroot.common.messaging.Queues.QUEUE_ALARM_CONFIG_DEL;

import com.centralroot.backendrestservice.entities.CustomAlarmCondition;
import com.centralroot.backendrestservice.entities.EdgeDevice;
import com.centralroot.backendrestservice.entities.Role;
import com.centralroot.backendrestservice.entities.User;
import com.centralroot.backendrestservice.entities.UserSetting;
import com.centralroot.backendrestservice.messaging.MessageSender;
import com.centralroot.backendrestservice.repository.DeviceRepository;
import com.centralroot.backendrestservice.repository.UserRepository;
import com.centralroot.backendrestservice.repository.UserSettingRepository;
import com.centralroot.common.model.AlarmConfigId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserSettingRepository userSettingRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private SessionRegistry sessionRegistry;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private ApplicationContext ac;

    @Autowired
    private PersistentTokenRepository tokenRepository;

    @Autowired
    private AlarmService alarmService;

    @Autowired
    private MessageSender messageSender;

    @Override
    public void save(User user) {
        if (userRepository.findByUsername(user.getUsername()) == null){
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            Role role = roleService.findByRoleName("ROLE_USER");
            user.setRoles(new HashSet<Role>(Collections.singleton(role)));
            userRepository.save(user);
        }
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findNormalUsers(Collection<String> roles) {
        return userRepository.findByRolesNotIn(roles);
    }

    private List<String> findSessionIdsByUsername(String username){
        List<Object> allPrincipals = sessionRegistry.getAllPrincipals();
        List<String> allSessionId = new ArrayList<>();

        for (final Object principal : allPrincipals) {
            if (principal instanceof org.springframework.security.core.userdetails.User) {
                org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) principal;
                if (username.equals(user.getUsername())) {
                    List<SessionInformation> activeUserSessions = sessionRegistry.getAllSessions(principal, false);
                    for (SessionInformation sessionInformation : activeUserSessions){
                        allSessionId.add(sessionInformation.getSessionId());
                    }
                    return allSessionId;
                }
            }
        }
        return allSessionId;
    }

    @Override
    public int deleteNormalUserByUsername(String username) {
        int delete = userRepository.deleteByUsername(username);
        tokenRepository.removeUserTokens(username);

        List<String> sessionIds = findSessionIdsByUsername(username);
        for (String sessionId : sessionIds){
            sessionRegistry.getSessionInformation(sessionId).expireNow();
        }
        UserService sameBean = ac.getBean(UserService.class);
        sameBean.evictSingleUserSetting(username);

        return delete;
    }

    @Override
    public List<String> getAllLoggedinUsers() {
        return sessionRegistry.getAllPrincipals()
            .stream()
            .filter(u -> !sessionRegistry.getAllSessions(u, false)
                .isEmpty())
            .map(o -> ((org.springframework.security.core.userdetails.User) o).getUsername()).collect(Collectors.toList());
    }
    
    @Override
    @Cacheable(value = "usersettings", key = "#username")
    public UserSetting findUserSettingsByUsername(String username) {
        return userSettingRepository.findSettingsByUsername(username);
    }

    @Override
    @CachePut(value = "usersettings", key = "#userSetting.user.username")
    public UserSetting saveUserSetting(UserSetting userSetting){
        return userSettingRepository.save(userSetting);
    }

    @CacheEvict(value = "usersettings", key = "#username")
    public void evictSingleUserSetting(String username) {}

    @Override
    @Transactional
    public int deleteSettingsByUsername(String username){
        int delete = userSettingRepository.deleteByUsername(username);
        UserService sameBean = ac.getBean(UserService.class);
        sameBean.evictSingleUserSetting(username);
        return delete;
    }

    @Override
    public List<User> findUsersByDeviceId(String deviceId) {
        return userRepository.findUsersByDeviceId(deviceId);
    }

    @Override
    public void assignDeviceToUser(String deviceId, List<String> usernames) {
        EdgeDevice edgeDevice = deviceRepository.findByDeviceId(deviceId);
        List<User> assigneeUsers = userRepository.findUsersByDeviceId(deviceId);
        List<String> assigneeUsernames = assigneeUsers.stream().map(User::getUsername).collect(Collectors.toList());

        // Users to assign devices
        List<String> usernamesToAssign = ListUtils.subtract(usernames, assigneeUsernames);
        for(String username : usernamesToAssign){
            User user = userRepository.findByUsername(username);
            user.getEdgedevices().add(edgeDevice);
            userRepository.save(user);
        }

        // Users to unassign devices
        List<String> usernamesToUnassign = ListUtils.subtract(assigneeUsernames, usernames);
        for (String username : usernamesToUnassign) {
            // Gets user's active alarm conditions
            List<CustomAlarmCondition> alarmConditions = alarmService
                .findAlarmConditionsByUsernameAndDeviceId(username, deviceId);
            // Sets user's active condition inactive and removes from alarm service
            for (CustomAlarmCondition condition: alarmConditions){
                messageSender.sendMessage(QUEUE_ALARM_CONFIG_DEL, new AlarmConfigId(condition.getConfigId()));
                condition.setActive(false);
                alarmService.saveAlarmCondition(condition);
            }
            // removes the device from the user's device table
            User user = userRepository.findByUsername(username);
            user.getEdgedevices().remove(edgeDevice);
            userRepository.save(user);
            deleteSettingsByUsername(username);
        }
    }
}
