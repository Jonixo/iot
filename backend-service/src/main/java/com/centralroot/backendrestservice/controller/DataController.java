package com.centralroot.backendrestservice.controller;

import com.centralroot.backendrestservice.model.AggregatedQueryData;
import com.centralroot.backendrestservice.model.DataTrendOptions;
import com.centralroot.backendrestservice.service.DataService;
import java.security.Principal;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.util.LinkedList;

@Controller
public class DataController {

  @Autowired
  private DataService dataService;

  @MessageMapping("/trends")
  @SendToUser("/queue/trends")
  public LinkedList<AggregatedQueryData> getDataTrends(@Valid @Payload DataTrendOptions options, Principal user) {
    return dataService.getAggregatedData(options, user);
  }

  @MessageExceptionHandler()
  @SendToUser(value = "/queue/trends", broadcast = false)
  public LinkedList<AggregatedQueryData> handleException(Exception e){
    return new LinkedList<>();
  }
}
