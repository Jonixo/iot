package com.centralroot.backendrestservice.controller;

import com.centralroot.backendrestservice.entities.User;
import com.centralroot.backendrestservice.service.DeviceService;
import com.centralroot.backendrestservice.service.UserService;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AdminController {
    @Autowired
    private UserService userService;

    @Autowired
    private DeviceService deviceService;

    @GetMapping("/admin")
    public String getDashboardPage(Model model) {
        List<String> loggedinUsers = userService.getAllLoggedinUsers();
        loggedinUsers.remove("cradmin");
        List<User> allUsers = userService.findNormalUsers(Arrays.asList("ROLE_ADMIN", "ROLE_ACTUATOR"));
        model.addAttribute("loggedinUsers", loggedinUsers);
        model.addAttribute("allUsers", allUsers.size());

        return "admin/dashboard";
    }

    @GetMapping("/user-operations")
    public String getUserOperationsPage(Model model) {
        // Returns Users who does not have "ROLE_ADMIN", "ROLE_ACTUATOR"
        List<User> allUsers = userService.findNormalUsers(Arrays.asList("ROLE_ADMIN", "ROLE_ACTUATOR"));

        model.addAttribute("users", allUsers);
        model.addAttribute("user", new User());
        return "admin/user_operations";
    }

    @GetMapping("/device-operations")
    public String getDeviceOperationsPage(Model model) {
        // Returns Users who does not have "ROLE_ADMIN", "ROLE_ACTUATOR"
        List<User> allUsers = userService.findNormalUsers(Arrays.asList("ROLE_ADMIN", "ROLE_ACTUATOR"));
        model.addAttribute("users", allUsers);
        model.addAttribute("devices", deviceService.getAllDevices());
        return "admin/device_operations";
    }

    @DeleteMapping("/user/{username}")
    public @ResponseBody int deleteUser(@PathVariable @NotBlank @Size(min = 6, max = 15)
            @Pattern(regexp = "^[a-zA-Z0-9]*$") String username) {

        return userService.deleteNormalUserByUsername(username);
    }

    @PostMapping("/user")
    public String saveUser(@Valid @ModelAttribute("user") User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            // Returns Users who does not have "ROLE_ADMIN", "ROLE_ACTUATOR"
            List<User> allUsers = userService.findNormalUsers(Arrays.asList("ROLE_ADMIN", "ROLE_ACTUATOR"));

            model.addAttribute("users", allUsers);
            model.addAttribute("user", user);
            return "admin/user_operations";
        }
        userService.save(user);
        return "redirect:/user-operations";
    }

    @GetMapping("/device/{deviceId}")
    @ResponseBody
    public List<String> getUsernamesOfAssignedDevice(@PathVariable @NotBlank
            @Pattern(regexp = "^[a-zA-Z0-9-]*$") @Size(min = 12, max = 32) String deviceId){

        List<User> users = userService.findUsersByDeviceId(deviceId);
        return users.stream().map(User::getUsername).collect(Collectors.toList());
    }

    @PutMapping("/device/{deviceId}")
    @ResponseBody
    public void assignDeviceToUser(@RequestBody Map<String, List<String>> usernames,
            @PathVariable @NotBlank @Pattern(regexp = "^[a-zA-Z0-9-]*$")
            @Size(min = 12, max = 32) String deviceId) {

        userService.assignDeviceToUser(deviceId, usernames.get("usernames"));
    }
}
