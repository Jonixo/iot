package com.centralroot.backendrestservice.controller;

import com.centralroot.backendrestservice.entities.CustomAlarmCondition;
import com.centralroot.backendrestservice.entities.User;
import com.centralroot.backendrestservice.entities.UserSetting;
import com.centralroot.backendrestservice.service.AlarmService;
import com.centralroot.backendrestservice.service.DeviceService;
import com.centralroot.backendrestservice.service.UserService;
import java.security.Principal;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private UserService userService;

    @Autowired
    private AlarmService alarmService;

    @GetMapping(value = {"/", "/dashboard"})
    public String getDashboardPage(Model model, Principal user) {
        model.addAttribute("unreadAlarmSize", alarmService.getNumberOfUnreadAlarms(user.getName()));
        model.addAttribute("alarms", alarmService.getLastTenAlarms(user.getName()));
        model.addAttribute("devices", deviceService.getUserDevices(user.getName()));
        model.addAttribute("usersettings", userService.findUserSettingsByUsername(user.getName()));
        return "dashboard";
    }

    @GetMapping("/trends")
    public String getTrendsPage(Model model, Principal user) {
        model.addAttribute("unreadAlarmSize", alarmService.getNumberOfUnreadAlarms(user.getName()));
        model.addAttribute("devices", deviceService.getUserDevices(user.getName()));
        return "trends";
    }

    @GetMapping("/devices")
    public String getDevicesPage(Model model, Principal user) {
        model.addAttribute("unreadAlarmSize", alarmService.getNumberOfUnreadAlarms(user.getName()));
        model.addAttribute("devices", deviceService.getUserDevices(user.getName()));
        return "devices";
    }

    @PutMapping("/user-settings/{deviceId}")
    @ResponseBody
    public UserSetting setDefaultRealtimeDevice(@Valid @RequestBody UserSetting userSetting,
            @NotBlank @Pattern(regexp = "^[a-zA-Z0-9-]*$") @Size(min = 12, max = 32)
            @PathVariable String deviceId, Principal user) {

        if (deviceService.existsByDeviceIdAndUsername(user.getName(), deviceId)) {
            User userInfo = userService.findByUsername(user.getName());
            userSetting.setUser(userInfo);

            UserSetting userSettingCurrent = userService.findUserSettingsByUsername(userInfo.getUsername());
            if (null != userSettingCurrent) {
                userSettingCurrent.setUser(userInfo);
                userSettingCurrent.setDeviceId(userSetting.getDeviceId());
                userSettingCurrent.setDeviceTag(userSetting.getDeviceTag());
                userSettingCurrent.setSensorType(userSetting.getSensorType());
                return userService.saveUserSetting(userSettingCurrent);
            } else {
                return userService.saveUserSetting(userSetting);
            }
        }
        return new UserSetting();
    }

    @GetMapping("/alarm-config")
    public String getAlarmConfigPage(Model model, Principal user) {
        model.addAttribute("unreadAlarmSize", alarmService.getNumberOfUnreadAlarms(user.getName()));
        model.addAttribute("alarm_conditions", alarmService.findAlarmConditionsByUsername(user.getName()));
        model.addAttribute("devices", deviceService.getUserDevices(user.getName()));
        model.addAttribute("config", new CustomAlarmCondition());
        return "alarm_config";
    }

    @DeleteMapping("/alarm-config/{configId}")
    @ResponseBody
    public CustomAlarmCondition deleteUser(@PathVariable @NotBlank String configId) {

        return alarmService.deleteAlarmConfigByConfigId(configId);
    }

    @PostMapping("/alarm-config")
    public String saveAlarmConfig(@Valid @ModelAttribute("config") CustomAlarmCondition condition,
        BindingResult bindingResult, Model model, Principal user){
        if (bindingResult.hasErrors()) {
            model.addAttribute("unreadAlarmSize", alarmService.getNumberOfUnreadAlarms(user.getName()));
            model.addAttribute("alarm_conditions", alarmService.findAlarmConditionsByUsername(user.getName()));
            model.addAttribute("devices", deviceService.getUserDevices(user.getName()));
            model.addAttribute("config", condition);
            return "alarm_config";
        }
        alarmService.saveAlarmCondition(condition, user.getName());
        return "redirect:/alarm-config";
    }

    @GetMapping("/alarms")
    public String getAlarmsPage(Model model, Principal user) {
        model.addAttribute("unreadAlarmSize", alarmService.getNumberOfUnreadAlarms(user.getName()));
        model.addAttribute("alarms", alarmService.getAllAlarms(user.getName()));
        return "alarms";
    }

    @GetMapping("/alarms/{alarmId}")
    public String setAlarmAsChecked(@NotBlank @Pattern(regexp = "^[a-zA-Z0-9-]*$") @Size(min = 12, max = 32)
        @PathVariable String alarmId, Principal user) {
        alarmService.setAlarmChecked(alarmId, user.getName());
        return "redirect:/alarms";
    }
}
