package com.centralroot.backendrestservice.controller;

import com.centralroot.backendrestservice.model.DeviceSystemInfo;
import com.centralroot.backendrestservice.service.DeviceService;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

@Controller
public class DeviceController {
  @Autowired
  private DeviceService deviceService;

  @MessageMapping("/deviceinfo")
  @SendToUser("/queue/deviceinfo")
  public DeviceSystemInfo getDeviceSysInfo(@Payload String deviceId, Principal user) {
    return deviceService.getDeviceSystemInfo(deviceId.replace("\"", ""), user);
  }
}
