package com.centralroot.backendrestservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendRestServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendRestServiceApplication.class, args);
    }

}
