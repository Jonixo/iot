package com.centralroot.backendrestservice.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private static final Logger LOG = LoggerFactory.getLogger(CustomAuthenticationSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication){

        Set<String> authorities = AuthorityUtils.authorityListToSet(authentication.getAuthorities());

        if(authorities.contains("ROLE_USER")) {
            try {
                redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/dashboard");
            } catch (Exception e) {
                LOG.info(String.format("Redirect exception: %s", e));
            }
        }
        else if(authorities.contains("ROLE_ADMIN")) {
            try {
                redirectStrategy.sendRedirect(httpServletRequest, httpServletResponse, "/admin");
            } catch (Exception e) {
                LOG.info(String.format("Redirect exception: %s", e));
            }
        }
        else {
            throw new IllegalStateException();
        }
    }

}
