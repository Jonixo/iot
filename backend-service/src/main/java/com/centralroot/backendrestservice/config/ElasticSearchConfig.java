package com.centralroot.backendrestservice.config;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.io.IOException;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "spring")
public class ElasticSearchConfig {
    @Value("${elasticsearch.username}")
    private String elasticUsername;

    @Value("${elasticsearch.password}")
    private String elasticPassword;

    @Value("${elasticsearch.host}")
    private String elasticHost;

    @Value("${elasticsearch.port}")
    private int elasticPort;

    private RestHighLevelClient client;

    private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchConfig.class);

    @Bean(destroyMethod = "close")
    public RestHighLevelClient client() {
        //Credentials for Elasticsearch
        final CredentialsProvider credentialsProvider =
                new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(elasticUsername, elasticPassword));

        //Elasticsearch Client
        RestClientBuilder builder = RestClient.builder(
                new HttpHost(elasticHost, elasticPort))
                .setHttpClientConfigCallback(httpClientBuilder -> {
                    httpClientBuilder.disableAuthCaching();
                    return httpClientBuilder
                            .setDefaultCredentialsProvider(credentialsProvider);
                });

        this.client = new RestHighLevelClient(builder);

        return this.client;
    }

    @PreDestroy
    public void cleanup() {
        try {
            LOG.info("Closing the ES REST client");
            this.client.close();
        } catch (IOException ioe) {
            LOG.error("Problem occurred when closing the ES REST client", ioe);
        }
    }

}
