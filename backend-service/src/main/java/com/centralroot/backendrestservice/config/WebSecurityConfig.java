package com.centralroot.backendrestservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Qualifier("userDetailsServiceImpl")
    @Autowired
    private UserDetailsService customUserDetailsService;

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Bean
    public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
        return new ServletListenerRegistrationBean<>(new HttpSessionEventPublisher());
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(bCryptPasswordEncoder());
    }

    @Autowired
    private AuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/dist/**", "/plugins/**").permitAll()
                .antMatchers("/dashboard", "/").access("hasRole('ROLE_USER')")
                .antMatchers("/admin", "/admin/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/app/**").access("hasRole('ROLE_USER')")
                .antMatchers("/cr-websocket").access("hasRole('ROLE_USER')")
                .antMatchers("/topic/**").access("hasRole('ROLE_USER')")
                .antMatchers("/trend").access("hasRole('ROLE_USER')")
                .antMatchers("/devices").access("hasRole('ROLE_USER')")
                .antMatchers("/device", "/device/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/trends").access("hasRole('ROLE_USER')")
                .antMatchers("/settings").access("hasRole('ROLE_USER')")
                .antMatchers("/login").access("hasRole('ROLE_ANONYMOUS')")
                .antMatchers("/user/**").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/webjars/**").permitAll()
                .anyRequest().authenticated() // rest of the endpoints is accessible to authenticated ones only
                .and()
                    .formLogin().loginPage("/login")
                    .successHandler(customAuthenticationSuccessHandler)
                    .failureUrl("/login?error")
                    .permitAll()
                .and()
                .sessionManagement()
                    .invalidSessionUrl("/login")
                    .maximumSessions(1).sessionRegistry(sessionRegistry()).and()
                    .sessionFixation().none()
                .and()
                    .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                    .logoutSuccessUrl("/login?logout")
                    .deleteCookies("my-remember-me-cookie")
                    // Since logout operation invalidates the session,
                    // it does not apply logout success url but invalid session url
                    .invalidateHttpSession(true)
                    .permitAll()
                .and()
                    .rememberMe()
                    //.key("my-secure-key")
                    .rememberMeCookieName("my-remember-me-cookie")
                    .tokenRepository(persistentTokenRepository())
                    .tokenValiditySeconds(9 * 60 * 60)
                .and()
                    .exceptionHandling();
    }

    @Bean
    PersistentTokenRepository persistentTokenRepository(){
        JdbcTokenRepositoryImpl tokenRepositoryImpl = new JdbcTokenRepositoryImpl();
        tokenRepositoryImpl.setDataSource(dataSource);
        return tokenRepositoryImpl;
    }
}