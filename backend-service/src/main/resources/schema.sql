create table if not exists persistent_logins
(
    username    varchar(100) not null,
    series      varchar(64) primary key,
    token       varchar(64) not null,
    last_used   timestamp not null
);

create table users
(
    id         bigint not null primary key,
    first_name varchar(255) not null,
    last_name  varchar(255) not null,
    password   varchar(255) not null,
    username   varchar(255) not null unique
);

create table roles
(
    id   bigint       not null
        constraint roles_pkey
            primary key,
    name varchar(255) not null
);

create table users_role
(
    user_id bigint not null
        references users,
    role_id bigint not null
        references roles,
    constraint users_role_pkey
        primary key (user_id, role_id)
);

create table edgedevices
(
    id           bigint       not null
        primary key,
    description  varchar(255) not null,
    device_id    varchar(255) not null unique,
    sensor_names text[],
    tag          varchar(255) not null
);

create table users_device
(
    user_id   bigint not null
        references users,
    device_id bigint not null
        references edgedevices,
    constraint users_device_pkey
        primary key (user_id, device_id)
);

create table device_credentials
(
    id         bigint       not null primary key,
    credential varchar(255) not null,
    protocol   varchar(255) not null,
    device_id  bigint       not null unique
        references edgedevices
);

create table usersettings
(
    id          bigint not null
        constraint usersettings_pkey
            primary key,
    device_id   varchar(255),
    device_tag  varchar(255),
    sensor_type varchar(255),
    user_id     bigint not null unique
            references users
);

create table alarm_conditions
(
    id          bigint       not null
        constraint alarm_conditions_pkey
            primary key,
    active      boolean      not null,
    config_id   varchar(255) not null unique,
    description varchar(255) not null,
    device_id   varchar(255) not null,
    level       varchar(255) not null,
    sensor_type varchar(255) not null,
    type        varchar(255) not null,
    value       numeric(10, 2) not null,
    username    varchar(255) not null
            references users (username)
);

create table alarms
(
    id             bigint       not null
        constraint alarms_pkey
            primary key,
    alarm_id       varchar(255) not null unique,
    timestamp      bigint       not null,
    checked        boolean      not null,
    data_timestamp bigint       not null,
    value          numeric(10, 2) not null,
    config_id      varchar(255) not null
            references alarm_conditions (config_id)
);