INSERT INTO roles (id, name) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_ACTUATOR'),
(3, 'ROLE_USER');

INSERT INTO users (id, username, password, first_name, last_name) VALUES
(1, 'cradmin', '$2a$10$HYnqC4il1rx3OyNi7Mto..jrjOxz4zrn9gp6UX2N70rcdQznGQTG.', 'CentralRoot', 'Admin'),
(2, 'cruser', '$2a$10$zZwCFTO7lRvxiZ1tkZJyauA5zxowTvdSgROwmA49LHtXTdCIYF8JG', 'CentralRoot', 'User');

insert into users_role(user_id, role_id) values
(1, 1),
(1, 2),
(2, 3);

insert into edgedevices(id, description, device_id, sensor_names, tag) values
(1, 'Raspberry Pi 4 demo device', 'c735cefd8ea1', '{"Temperature", "Humidity"}', 'Raspberry-M'),
(2, 'Test simulation script MQTT', '54ef3a00d93c', '{"Temperature", "Voltage", "Pressure"}', 'Script-M'),
(3, 'Test simulation script REST', 'drmx9wbtyw5f', '{"Light", "Gas"}', 'Script-R');

insert into device_credentials(id, device_id, protocol, credential) values
(1, 1, 'MQTT', 'vkRnc2a6R6ARuo6A'),
(2, 2, 'MQTT', 'cBOhpQ1mwgTeQ9DE'),
(3, 3, 'MQTT', 'Tj8FciNtBgE0EGgJ');

insert into users_device(user_id, device_id) values
(2, 1),
(2, 2),
(2, 3);
